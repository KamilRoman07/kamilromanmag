using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ImageDrawer : MonoBehaviour
{
    ImageDrawer instance;
    [Header("--Coord size bounds--")]
    [SerializeField] private int maksSize = 75;
    [SerializeField] private int minSize = 25;

    [Header("--Objects--")]
    [SerializeField] Image imageHolder;
    [SerializeField] GameObject textureScreen;

    [Header("--Miscellenius (for testing purposes)--")]
    [SerializeField] int HistogramIndex;

    //TextAsset[] files;
    List<Tuple<Vector2, Vector2>> coords = new List<Tuple<Vector2, Vector2>>();
    CultureInfo ci;

    long all = 0;
    long filtered = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }

        ClientHandle.onCoordsPacketReceived += ReciveCoord;

        ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
        ci.NumberFormat.CurrencyDecimalSeparator = ".";
        
        //TO DO - ADD WAITING FOR CONNECTION, REPEATED CONNECTION TRIES
        StartCoroutine(ConnectToServer());
        StartCoroutine(DrawImage());
    }

    private void OnDisable()
    {
        ClientHandle.onCoordsPacketReceived -= ReciveCoord;
    }

    #region Server stuff
    private Tuple<Vector2, Vector2> FilterCoord(string coordLine)
    {
        try
        {
            all++;
            string[] coords = coordLine.Split(' ');
            Vector2 p1 = new Vector2(float.Parse(coords[0], NumberStyles.Any, ci), float.Parse(coords[1], NumberStyles.Any, ci));
            Vector2 p2 = new Vector2(float.Parse(coords[2], NumberStyles.Any, ci), float.Parse(coords[3], NumberStyles.Any, ci));
            if (Vector2.Distance(p1, p2) < maksSize && Vector2.Distance(p1, p2) > minSize)
                return new Tuple<Vector2, Vector2>(p1, p2);
            else
            {
                filtered++;
                return null;
            }

        }
        catch (Exception e)
        {
            Debug.LogError(e);
            return null;
        }
    }

    public IEnumerator ConnectToServer()
    {
        while (Client.instance == null)
            yield return new WaitForEndOfFrame();

        while (!Client.instance.tcpCreated)
            yield return new WaitForEndOfFrame();

        Client.instance.ConnectToServer();
    }

    private void ReciveCoord(string coordLine)
    {
        Tuple<Vector2, Vector2> deciphredCoord = FilterCoord(coordLine);
        if (deciphredCoord != null)
            coords.Add(deciphredCoord);
    }
    #endregion

    #region Draw
    private IEnumerator DrawImage()
    {
        yield return new WaitForSecondsRealtime(1f);
        if (coords.Count > 0)
        {
            CreateAndSetTexture();
        }
        else
        {
            Debug.LogError("Coords length = 0, did conneciton stopped?");
        }
        StartCoroutine(DrawImage());
    }

    private void CreateAndSetTexture()
    {
        Texture2D texture = new Texture2D(100, 100);
        texture = FillTextureWithColor(texture);
        foreach (Tuple<Vector2, Vector2> coordinate in coords)
            DrawLine(texture, coordinate.Item1, coordinate.Item2);

        texture.Apply();
        //Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        //imageHolder.sprite = sprite;
        textureScreen.GetComponent<Renderer>().material.mainTexture = texture;
    }

    private Texture2D FillTextureWithColor(Texture2D texture)
    {
        Color32 fillColor = new Color(0, 0, 0, 0);
        var fillColorArray = texture.GetPixels();

        for (var i = 0; i < fillColorArray.Length; ++i)
        {
            fillColorArray[i] = fillColor;
        }

        texture.SetPixels(fillColorArray);

        texture.Apply();
        return texture;
    }

    private void DrawLine(Texture2D tex, Vector2 p1, Vector2 p2)
    {
        Vector2 t = p1;
        float frac = 1 / Mathf.Sqrt(Mathf.Pow(p2.x - p1.x, 2) + Mathf.Pow(p2.y - p1.y, 2));
        float ctr = 0;

        while ((int)t.x != (int)p2.x || (int)t.y != (int)p2.y)
        {
            t = Vector2.Lerp(p1, p2, ctr);
            ctr += frac;
            tex.SetPixel((int)t.x, (int)t.y, DecideColor(tex, (int)t.x, (int)t.y));
        }
    }

    private Color DecideColor(Texture2D tex, int x, int y)
    {
        Color32 pixelColor =  tex.GetPixel(x, y);
        if (pixelColor.Equals(new Color32(0, 0, 0, 255)))
            return new Color32(0, 0, 255, 255);
        else if (pixelColor.b > 0)
            return new Color32(Convert.ToByte(pixelColor.r + 1), Convert.ToByte(pixelColor.g + 1), Convert.ToByte(pixelColor.b - 1), 255);
        else if (pixelColor.g > 0 && pixelColor.b == 0)
            return new Color32(255, Convert.ToByte(pixelColor.g - 1), 0, 255);
        else
            return new Color32(0, 0, 0, 255);
    }
    #endregion

    #region Unused for now
    private List<Tuple<Vector2, Vector2>> FilterCoords(string[] lines)
    {
        List<Tuple<Vector2, Vector2>> deciphredCoords = new List<Tuple<Vector2, Vector2>>();
        all = lines.Length;
        foreach (string line in lines)
        {
            try
            {
                string[] coords = line.Split(' ');
                Vector2 p1 = new Vector2(float.Parse(coords[0], NumberStyles.Any, ci), float.Parse(coords[1], NumberStyles.Any, ci));
                Vector2 p2 = new Vector2(float.Parse(coords[2], NumberStyles.Any, ci), float.Parse(coords[3], NumberStyles.Any, ci));
                if (Vector2.Distance(p1, p2) < maksSize && Vector2.Distance(p1, p2) > minSize)
                    deciphredCoords.Add(new Tuple<Vector2, Vector2>(p1, p2));
                else
                {
                    //Debug.Log("Rejected line of coords: " + p1 + ", " + p2);
                    filtered++;
                }

            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
        Debug.LogError("Filtered " + filtered + " out of" + all);
        return deciphredCoords;
    }

    #endregion
}