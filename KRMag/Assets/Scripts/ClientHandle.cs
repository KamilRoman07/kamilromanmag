using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientHandle : MonoBehaviour
{
    public static Action<string> onCoordsPacketReceived;
    static int received = 0;

    public static void Message(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();

        Debug.Log($"Message from server: {_msg}");
        Client.instance.myId = _myId;
    }

    public static void CoordsPacket(Packet _packet)
    {
        string coordLine = _packet.ReadString();
        int _myId = _packet.ReadInt();

        Client.instance.myId = _myId;
        onCoordsPacketReceived?.Invoke(coordLine);
    }
}